## Debian for DEVELOPMENT.

[![Docker Build Status](https://img.shields.io/docker/build/cn007b/debian.svg)](https://hub.docker.com/r/cn007b/debian/)
[![Docker Automated build](https://img.shields.io/docker/automated/cn007b/debian.svg)](https://hub.docker.com/r/cn007b/debian/)
[![Docker Pulls](https://img.shields.io/docker/pulls/cn007b/debian.svg)](https://hub.docker.com/r/cn007b/debian/)

This image contains pre-installed tools helpful for development purposes.

# List of installed tools:

* tree, mc, screen
* git, colordiff
* curl, wget, telnet
* zip, unzip
* make

# Usage:

````sh
docker run -ti --rm -v $PWD:/app -w /app cn007b/debian sh -c 'echo ok'

# or
docker run -ti --rm -v $PWD:/app -w /app cn007b/debian /bin/bash
````
